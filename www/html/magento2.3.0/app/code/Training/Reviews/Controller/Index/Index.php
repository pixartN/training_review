<?php

namespace Training\Reviews\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {

        $post = (array) $this->getRequest()->getPost();

        if (!empty($post)) {

            $nickname   = $post['nickname'];
            $textreview    = $post['textreview'];




            $this->messageManager->addSuccessMessage('Review Submit !');


            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/reviews/index/index');

            return $resultRedirect;
        }

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }



}

