<?php
namespace Training\Reviews\Model\ResourceModel;

class AllReviews extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function _construct()
    {
        $this->_init('training_reviews', 'id');
    }
}