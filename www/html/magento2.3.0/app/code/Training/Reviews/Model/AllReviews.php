<?php
namespace Training\Reviews\Model;

class AllReviews extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init('Training\Reviews\Model\ResourceModel\AllReviews');
    }
}