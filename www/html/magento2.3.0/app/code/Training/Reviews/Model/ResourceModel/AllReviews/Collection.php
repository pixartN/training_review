<?php
namespace Training\Reviews\Model\ResourceModel\AllReviews;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Training\Reviews\Model\AllReviews', 'Training\Reviews\Model\ResourceModel\AllReviews');
    }
}
